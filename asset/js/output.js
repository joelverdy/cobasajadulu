let dataOutput = [];
$("#summary-tab").click(function () {
    dataOutput = output;
    console.log(dataOutput);

    showDijawab(dataOutput);
    showRemark(dataOutput);
    showValid(metadata, dataOutput);
    showError(metadata, dataOutput);


});

function showDijawab(data) {
    document.getElementById("dijawab-tab").innerHTML = "Dijawab (" + data.length + ")";
    document.getElementById("card-dijawab").querySelector('h1').innerHTML = data.length;
}

function showRemark(data) {
    const dataRemark = data.filter(out => out.remark != "");
    const countRemark = dataRemark.length;

    document.getElementById("remark-tab").innerHTML = "Remark (" + countRemark + ")";
    document.getElementById("card-remark").querySelector('h1').innerHTML = countRemark;

}

function showValid() {
    // const dataValid = dataOutputFilterErrorBaru(metadata, data).map(x => x.metaJoin).filter(x => x.length == 0);
    // console.log('Data Valid :', dataValid);
    // let jumlahData = dataValid.length;

    document.getElementById("valid-tab").innerHTML = "Valid (" + 0 + ")";
    document.getElementById("card-valid").querySelector('h1').innerHTML = 0;
}

function showError() {
    // const dataInvalid = dataOutputFilterErrorBaru(metadata, data).map(x => x.metaJoin).filter(x => x.length > 0);
    // let jumlahData = dataInvalid.length;

    document.getElementById("error-tab").innerHTML = "Error (" + 0 + ")";
    document.getElementById("card-error").querySelector('h1').innerHTML = 0;
}

function dataOutputFilterErrorBaru(metadata, listofdata) {
    const listData = listofdata.map(data => {
        let metaJoin;
        const outMetaJoin = metadata.find(meta => meta.componentId == data.componentId);

        metaJoin = outMetaJoin.validation.map(validations => ({
            id: validations.id,
            rules: validations.rules.replace('__self', "'" + data.value + "'"),
            message: validations.message,
            type: validations.type,
            params: validations.params != undefined ? validations.params : false
        }));

        return {
            metaJoin
        };
    })

    return listData;
}

const countdijawab = 0;


if (countdijawab > 0) {
    //data remark
    const dataremark = output2.filter(out => out.remark != "");
    const countremark = dataremark.length;
    document.getElementById("card-remark").querySelector('h1').innerHTML = countremark;
    document.getElementById("remark-tab").innerHTML = "Remark (" + countremark + ")";


    //join data
    const joinMetaOutput = metadata.map(meta => ({
        ...meta,
        output: output2.filter(x => x.componentId == meta.componentId)
    }));

    //Ambil data output dan dicek
    const listDataErrorMentah = metadata.map(meta => {
        const outdatapermeta = output2.find(x => x.componentId == meta.componentId).value;


        const validasi = meta.validation.map(y => ({
            id: y.id,
            rules: y.rules.replace('__self', "'" + outdatapermeta + "'"),
            message: y.message,
            type: y.type,
            params: y.params != undefined ? y.params : false
        })).filter(y => !eval(y.rules));

        return {
            validasi
        };
    })

    //ambil data yang tadi cek mana list yang error
    const dataError = listDataErrorMentah.map(x => x.validasi).filter(x => x.length > 0);
    const countDataError = dataError.length;
    document.getElementById("card-error").querySelector('h1').innerHTML = countDataError;
    document.getElementById("error-tab").innerHTML = "Error (" + countDataError + ")";



    //ambil data yang tidak error (valid)
    const dataValid = listDataErrorMentah.map(x => x.validasi).filter(x => x.length == 0);
    const countDataValid = dataValid.length;
    document.getElementById("card-valid").querySelector('h1').innerHTML = countDataValid;
    document.getElementById("valid-tab").innerHTML = "Valid (" + countDataValid + ")";


}