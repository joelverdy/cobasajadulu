const metadata = [{
    componentId: '1baa2935-551c-42ce-8211-6f74ebb95c6a',
    componentType: {
      componentTypeId: '06eede23-bd1f-422a-9462-790c70a585e5',
      componentTypeName: "InputNumber"
    },
    dataKey: "no_ktp",
    required: true,
    hint: "",
    validation: [{
      id: "c058964b-1942-456d-89e2-ba7617264aef",
      rules: "__self.length == 16",
      message: "Nomor KTP Harus 16 digit angka",
      type: "soft"
    }],
    enablingCondition: null
  },
  {
    componentId: '7b5c5db7-86eb-4955-960f-3d68ae8ddf62',
    componentType: {
      componentTypeId: '6550aaf9-b36a-4c14-9305-0dab000a5979',
      componentTypeName: "InputText"
    },
    dataKey: "nama_lengkap",
    required: true,
    hint: "",
    validation: [{
      id: "25b4e120-1585-49d9-95e2-1944456a58b1",
      rules: "__self.length > 2",
      message: "Nama lengkap minimal 3 huruf",
      type: "soft"
    }, {
      id: "b209620e-838a-4647-a689-abfefc2b74ca",
      rules: "/^[a-zA-Z]+$/.test(__self[0])",
      message: "Nama harus diawali dengan huruf",
      type: "hard"
    }],
    enablingCondition: null
  },
  {
    componentId: '1bcd05fc-f4b0-48ad-83f3-f89dd66aaef1',
    dataKey: 'jenis_kelamin',
    componentType: {
      componentTypeId: '22b05553-c903-434d-8138-9eff71c2243a',
      componentTypeName: 'InputRadio'
    },
    required: true,
    hint: "",
    validation: [],
    enablingCondition: null
  },
  {
    componentId: 'e722d512-cc3b-4967-ab0b-53c363902dc4',
    dataKey: 'kota_lahir',
    componentType: {
      componentTypeId: '22b05553-c903-434d-8138-9eff71c2243a',
      componentTypeName: 'ComboBox'
    },
    values: [{
        "value": "1",
        "text": "Kota Bandung"
      },
      {
        "value": "2",
        "text": "DKI Jakarta"
      },
      {
        "value": "3",
        "text": "Kota Bogor"
      },
      {
        "value": "4",
        "text": "DI Yogyakarta"
      },
      {
        "value": "5",
        "text": "Kota Jambi"
      },
      {
        "value": "6",
        "text": "Kota Semarang"
      },
      {
        "value": "7",
        "text": "Kota Cirebon"
      }
    ],
    required: true,
    hint: "",
    validation: [],
    enablingCondition: null
  },
  {
    componentId: 'af5da887-c487-4b28-b97d-08708d24951e',
    dataKey: 'pendidikan',
    componentType: {
      componentTypeId: '22b05553-c903-434d-8138-9eff71c2243a',
      componentTypeName: 'ComboBox'
    },
    values: [{
        "value": "sd",
        "text": "SD (Sekolah Dasar)"
      },
      {
        "value": "smp",
        "text": "SMP (Sekolah Menengah Pertama)"
      },
      {
        "value": "sma",
        "text": "SMA (Sekolah Menengah Atas)"
      },
      {
        "value": "s1",
        "text": "S1 (Strata 1)"
      },
      {
        "value": "s2",
        "text": "S2 (Strata 2)"
      }
    ],
    required: true,
    hint: "",
    validation: [],
    enablingCondition: null
  },
  {
    componentId: 'af5da887-c487-4b28-b97d-08708d24951d',
    dataKey: 'status_nikah',
    componentType: {
      componentTypeId: '22b05553-c903-434d-8138-9eff71c2243a',
      componentTypeName: 'ComboBox'
    },
    values: [{
        value: "1",
        text: "Belum Menikah"
      },
      {
        value: "2",
        text: "Menikah"
      }
    ],
    required: true,
    hint: "",
    validation: [{
      id: "aef298ea-d0a1-44c0-bf3d-2a4e589104c2",
      rules: "!(__self == '2' && getAge(tanggal_lahir) <= 16)",
      params: [
        "tanggal_lahir" //$("input[name='tanggal_lahir]'").val()
      ],
      message: "Tidak sesuai dengan batas minimal umur menikah",
      type: "soft"
    }],
    enablingCondition: null
  },

  {
    componentId: '9eaa2dc7-eec2-44f0-9f7d-9b5dc79fb855',
    dataKey: 'jumlah_anak',
    componentType: {
      componentTypeId: '22b05553-c903-434d-8138-9eff71c2243a',
      componentTypeName: 'InputNumber'
    },
    required: true,
    hint: "",
    validation: [{
      id: '6c699080-7268-4676-91c5-6d5e5177dba5',
      rules: 'Number(__self) >=0',
      message: "Jumlah anak berupa angka dan tidak boleh minus."
    }],
    enablingCondition: {
      condition: "status_menikah == 1",
      parameters: [
        "status_menikah"
      ]
    },
  },

  {
    componentId: 'e722d512-cc3b-4967-ab0b-53c363902dc3',
    dataKey: 'status_domisili',
    componentType: {
      componentTypeId: '22b05553-c903-434d-8138-9eff71c2243a',
      componentTypeName: 'ComboBox'
    },
    values: [{
        "value": "1",
        "text": "Kota Bandung"
      },
      {
        "value": "2",
        "text": "DKI Jakarta"
      },
      {
        "value": "3",
        "text": "Kota Bogor"
      },
      {
        "value": "4",
        "text": "DI Yogyakarta"
      },
      {
        "value": "5",
        "text": "Kota Jambi"
      },
      {
        "value": "6",
        "text": "Kota Semarang"
      },
      {
        "value": "7",
        "text": "Kota Cirebon"
      }
    ],
    required: true,
    hint: "",
    validation: [],
    enablingCondition: null
  },
  {
    componentId: 'a041e491-663d-4f2f-9078-08da25b3c4a7',
    dataKey: 'lama_menempati',
    componentType: {
      componentTypeId: '22b05553-c903-434d-8138-9eff71c2243a',
      componentTypeName: 'InputNumber'
    },
    required: true,
    hint: "",
    validation: [{
        id: '6c699080-7268-4676-91c5-6d5e5177dba5',
        rules: 'Number(__self) > 0',
        message: "Lama Menempati Minimal 1 Tahun."
      },
      {
        id: '6c699080-7268-4676-91c5-6d5e5177dba5',
        rules: '/^\\d+$/.test(__self)',
        message: "lama menempati harus berupa angka."
      }
    ],
    enablingCondition: null
  },
  {
    componentId: '8eaa2dc7-eec2-44f0-8f7d-8b5dc79fb855',
    dataKey: 'jumlah_tanggungan',
    componentType: {
      componentTypeId: '22b05553-c903-434d-8138-9eff71c2243a',
      componentTypeName: 'InputNumber'
    },
    required: true,
    hint: "",
    validation: [{
      id: '6c699080-7268-4676-91c5-6d5e5177dba5',
      rules: '/^\\d+$/.test(__self)',
      message: "Jumlah tanggungan harus berupa angka."
    }],
    enablingCondition: null
  },
  {
    componentId: '209620e-838a-4647-a689-abfefc2b74ca',
    dataKey: 'telepon_rumah',
    componentType: {
      componentTypeId: '22b05553-c903-434d-8138-9eff71c2243a',
      componentTypeName: 'InputNumber'
    },
    required: false,
    hint: "",
    validation: [],
    enablingCondition: null
  },
  {
    componentId: 'c83e9603-ee1e-4146-8e41-24a1bd96f92e',
    dataKey: 'tanggal_lahir',
    componentType: {
      componentTypeId: '20eaec5f-29b0-4479-983e-4350b385af1c',
      componentTypeName: 'InputDate'
    },
    required: false,
    hint: "",
    validation: [],
    enablingCondition: null
  }
]

let output = [];


$(document).ready(function () {

  // console.log('ready');
  console.log('length metadata :', metadata)
  const component = metadata.map(meta => {
    switch (meta.componentType.componentTypeName) {
      case 'InputText':
        return new TextInput(meta)
      case 'InputNumber':
        return new NumberInput(meta)
      case 'InputRadio':
        return new RadioInput(meta)
      case 'ComboBox':
        return new ComboBox(meta)
      case 'InputDate':
        return new DateInput(meta)
    }
  })


  console.log(component);

  $("#submit").click(function () {
    console.log(output);
  });

});

class Component {
  constructor(meta) {
    Object.assign(this, meta, {
      isPristine: true
    });
  }

  get element() {
    return document.querySelector('[data-cid="' + this.componentId + '"]');
  }

  get property() {
    return $('[data-cid="' + this.componentId + '"] input');
  }

  get validationAction() {
    const value = this.value;

    const validation = this.validation.map(meta => ({
      id: meta.id,
      rules: meta.rules.replace('__self', "'" + value + "'"),
      message: meta.message,
      type: meta.type,
      params: meta.params != undefined ? meta.params : false
    }));

    return validation;
  }

  get dataValidations() {
    const value = this.value;

    const validation = this.validation.map(meta => ({
      id: meta.id,
      rules: meta.rules.replace('__self', "'" + value + "'"),
      message: meta.message,
      type: meta.type,
      params: meta.params != undefined ? meta.params : false
    }));

    return validation;
  }

  get dataWithParametersValidation() {
    const dataValidation = this.dataValidations.filter(meta => meta.params != false);

    return dataValidation;
  }

  get hasParametersValidation() {
    return this.dataWithParametersValidation.length > 0;
  }

  get actionValidation() {
    if (this.hasParametersValidation) {
      const arrParam = this.dataWithParametersValidation[0].params;

      let isNotValidDataWithParam;

      arrParam.forEach(element => {
        const valueParam = $('input[name=' + element + ']').val();

        isNotValidDataWithParam = this.validationAction.map(meta => ({
            id: meta.id,
            rules: meta.rules.replace(element, "'" + valueParam + "'"),
            message: meta.message,
            type: meta.type,
            params: meta.params
          }))
          .filter(x => !eval(x.rules)); //kalo true teh berarti error atau enggak sesuai si pengecekannya
      });

      return isNotValidDataWithParam;
    } else {
      let isNotValidData = this.dataValidations.filter(meta => !eval(meta.rules));

      return isNotValidData;
    }
  }

  get currentDateTime() {
    const currentdate = new Date();
    const datetime = currentdate.getFullYear() + "-" +
      (currentdate.getMonth() + 1) + "-" +
      currentdate.getDate() + " " +
      currentdate.getHours() + ":" +
      currentdate.getMinutes() + ":" +
      currentdate.getSeconds();

    return datetime;
  }

  get remarkcontrol() {
    return this.element.querySelector('textarea');
  }

  get valueremark() {
    return this.remarkcontrol.value;
  }

  createFeedBack(el, msg, nameclass) {
    Object.assign(el, {
      className: nameclass,
      textContent: msg
    });

    this.control.parentElement.appendChild(el);
  }

  objectDataOutput(componentId, dataKey, value, valRemark, paradatas) {

    var obj = {};

    var newObj = Object.assign(obj, {
      componentId: componentId,
      dataKey: dataKey,
      value: value,
      remark: valRemark,
      paradata: [
        paradatas
      ]
    });

    return newObj;
  }
}

class TextInput extends Component {
  constructor(meta) {
    super(meta)

    this.control.addEventListener('change', this.onChange.bind(this));
    this.remarkcontrol.addEventListener('change', this.onChangeRemark.bind(this));
  }

  get control() {
    return this.element.querySelector('input[type="text"]');
  }

  get value() {
    return this.control.value;
  }

  get valueValidation() {
    return this.validationAction;
  }

  onChange(event) {

    const array = this.actionValidation;
    //console.log(array)
    if (array.length > 0) {
      if (array[0].type != 'soft')
        this.showErrors();
      else
        this.showWarning();

    } else {
      this.hideErrors();
    }


    const paradatas = [];

    if (output.length > 0) {
      let dataLama = output.filter(x => x.componentId == this.componentId);

      if (dataLama.length > 0) {
        paradatas.push(this.currentDateTime);
        paradatas.push(this.value);

        var datapara = [];
        for (var i = 0; i < dataLama[0].paradata.length; i++) {
          datapara.push(dataLama[0].paradata[i]);
        }

        datapara.push(paradatas);

        var objBaru = {};

        var hasilObject = Object.assign(objBaru, {
          componentId: this.componentId,
          dataKey: this.dataKey,
          value: this.value,
          remark: this.valueremark,
          paradata: datapara
        });

        //update
        var objIndex = output.findIndex((obj => obj.componentId == this.componentId));
        output[objIndex] = hasilObject;
      } else {
        paradatas.push(this.currentDateTime);
        paradatas.push(this.value);

        var hasilObject = this.objectDataOutput(this.componentId, this.dataKey, this.value, this
          .valueremark, paradatas);

        output.push(hasilObject);
      }

    } else {
      paradatas.push(this.currentDateTime);
      paradatas.push(this.value);

      var hasilObject = this.objectDataOutput(this.componentId, this.dataKey, this.value, this
        .valueremark,
        paradatas);

      output.push(hasilObject);
    }
  }

  onChangeRemark(event) {
    let dataLama = output.filter(x => x.componentId == this.componentId);

    if (dataLama.length > 0) {

      var datapara = [];
      for (var i = 0; i < dataLama[0].paradata.length; i++) {
        datapara.push(dataLama[0].paradata[i]);
      }

      //datapara.push(paradatas);

      var objBaru = {};

      var hasilObject = Object.assign(objBaru, {
        componentId: this.componentId,
        dataKey: this.dataKey,
        value: this.value,
        remark: this.valueremark,
        paradata: datapara
      });

      //update
      var objIndex = output.findIndex((obj => obj.componentId == this.componentId));
      output[objIndex] = hasilObject;
    }
  }

  hideErrors() {
    this.control.classList.remove('is-warning');
    this.control.classList.remove('is-invalid');
    this.control.classList.add('is-valid');

    const hasFeedBack = this.element.querySelector('.invalid-feedback') !== null;
    const hasWFeedBack = this.element.querySelector('.warning-feedback') !== null;

    if (hasWFeedBack)
      this.element.getElementsByClassName('warning-feedback')[0].remove();

    if (hasFeedBack)
      this.element.getElementsByClassName('invalid-feedback')[0].remove();
  }

  showErrors() {
    this.control.classList.remove('is-warning');
    this.control.classList.remove('is-valid');
    this.control.classList.add('is-invalid');


    const hasFeedBack = this.element.querySelector('.invalid-feedback') !== null;
    const hasWFeedBack = this.element.querySelector('.warning-feedback') !== null;

    if (hasWFeedBack)
      this.element.getElementsByClassName('warning-feedback')[0].remove();

    if (!hasFeedBack) {
      const invalidFeedback = document.createElement('div');

      this.createFeedBack(invalidFeedback, this.valueValidation[0].message,
        'invalid-feedback');
    } else {

      this.control.parentElement.lastChild.remove();

      const invalidFeedback = document.createElement('div');

      this.createFeedBack(invalidFeedback, this.valueValidation[0].message,
        'invalid-feedback');
    }
  }

  showWarning() {
    this.control.classList.remove('is-valid');
    this.control.classList.remove('is-invalid');
    this.control.classList.add('is-warning');

    const hasFeedBack = this.element.querySelector('.invalid-feedback') !== null;
    const hasWFeedBack = this.element.querySelector('.warning-feedback') !== null;

    if (hasFeedBack)
      this.element.getElementsByClassName('invalid-feedback')[0].remove();

    if (!hasWFeedBack) {
      const warningFeedback = document.createElement('div');

      this.createFeedBack(warningFeedback, this.valueValidation[0].message, 'warning-feedback');
    } else {
      this.control.parentElement.lastChild.remove();

      const warningFeedback = document.createElement('div');

      this.createFeedBack(warningFeedback, this.valueValidation[0].message, 'warning-feedback');
    }

  }


}

class NumberInput extends Component {
  constructor(meta) {
    super(meta)

    this.property.prop('type', 'number');

    this.control.addEventListener('change', this.onChange.bind(this));
    this.remarkcontrol.addEventListener('change', this.onChangeRemark.bind(this));
  }

  get control() {
    return this.element.querySelector('input[type="number"]');
  }

  get value() {
    return this.control.value;
  }

  get valueValidation() {
    return this.validationAction;
  }

  onChange() {

    const array = this.actionValidation;

    if (array.length > 0) {

      if (array[0].type != 'soft')
        this.showErrors();
      else
        this.showWarning();
    } else {
      this.hideErrors();
    }

    const obj = {};

    const paradatas = [];

    if (output.length > 0) {
      let dataLama = output.filter(x => x.componentId == this.componentId);

      if (dataLama.length > 0) {
        paradatas.push(this.currentDateTime);
        paradatas.push(this.value);

        var datapara = [];
        for (var i = 0; i < dataLama[0].paradata.length; i++) {
          datapara.push(dataLama[0].paradata[i]);
        }

        datapara.push(paradatas);

        var objBaru = {};

        var hasilObject = Object.assign(objBaru, {
          componentId: this.componentId,
          dataKey: this.dataKey,
          value: this.value,
          remark: this.valueremark,
          paradata: datapara
        });

        //update
        var objIndex = output.findIndex((obj => obj.componentId == this.componentId));
        output[objIndex] = hasilObject;
      } else {
        paradatas.push(this.currentDateTime);
        paradatas.push(this.value);


        var hasilObject = this.objectDataOutput(this.componentId, this.dataKey, this.value, this
          .valueremark,
          paradatas);

        output.push(hasilObject);
      }

    } else {
      paradatas.push(this.currentDateTime);
      paradatas.push(this.value);

      var hasilObject = this.objectDataOutput(this.componentId, this.dataKey, this.value, this
        .valueremark,
        paradatas);

      output.push(hasilObject);
    }
  }

  onChangeRemark(event) {
    let dataLama = output.filter(x => x.componentId == this.componentId);

    if (dataLama.length > 0) {

      var datapara = [];
      for (var i = 0; i < dataLama[0].paradata.length; i++) {
        datapara.push(dataLama[0].paradata[i]);
      }

      //datapara.push(paradatas);

      var objBaru = {};

      var hasilObject = Object.assign(objBaru, {
        componentId: this.componentId,
        dataKey: this.dataKey,
        value: this.value,
        remark: this.valueremark,
        paradata: datapara
      });

      //update
      var objIndex = output.findIndex((obj => obj.componentId == this.componentId));
      output[objIndex] = hasilObject;
    }
  }

  hideErrors() {
    this.control.classList.remove('is-invalid');
    this.control.classList.remove('is-warning');
    this.control.classList.add('is-valid');

    const hasFeedBack = this.element.querySelector('.invalid-feedback') !== null;
    const hasWFeedBack = this.element.querySelector('.warning-feedback') !== null;

    if (hasWFeedBack)
      this.element.getElementsByClassName('warning-feedback')[0].remove();

    if (hasFeedBack)
      this.element.getElementsByClassName('invalid-feedback')[0].remove();
  }

  showErrors() {
    this.control.classList.remove('is-warning');
    this.control.classList.remove('is-valid');
    this.control.classList.add('is-invalid');
    const hasFeedBack = this.element.querySelector('.invalid-feedback') !== null;
    const hasWFeedBack = this.element.querySelector('.warning-feedback') !== null;

    if (hasWFeedBack)
      this.element.getElementsByClassName('warning-feedback')[0].remove();

    if (!hasFeedBack) {
      const invalidFeedback = document.createElement('div');

      this.createFeedBack(invalidFeedback, this.valueValidation[0].message,
        'invalid-feedback');
    } else {

      this.control.parentElement.lastChild.remove();

      const invalidFeedback = document.createElement('div');

      this.createFeedBack(invalidFeedback, this.valueValidation[0].message,
        'invalid-feedback');
    }
  }

  showWarning() {
    this.control.classList.remove('is-invalid');
    this.control.classList.remove('is-valid');
    this.control.classList.add('is-warning');

    const hasFeedBack = this.element.querySelector('.invalid-feedback') !== null;
    const hasWFeedBack = this.element.querySelector('.warning-feedback') !== null;

    if (hasFeedBack)
      this.element.getElementsByClassName('invalid-feedback')[0].remove();

    if (!hasWFeedBack) {
      const warningFeedback = document.createElement('div');

      this.createFeedBack(warningFeedback, this.valueValidation[0].message, 'warning-feedback');
    } else {
      this.control.parentElement.lastChild.remove();

      const warningFeedback = document.createElement('div');

      this.createFeedBack(warningFeedback, this.valueValidation[0].message, 'warning-feedback');
    }

  }

}

class RadioInput extends Component {
  constructor(meta) {
    super(meta)

    this.value = '';

    for (var i = 0; i < this.control.length; i++) {
      this.control[i].addEventListener('change', this.radioChange.bind(this), false);
    }
    this.remarkcontrol.addEventListener('change', this.onChangeRemark.bind(this));
  }

  get control() {
    return this.element.querySelectorAll('input[type="radio"]');
  }

  get controlChecked() {
    return this.element.querySelectorAll('input[type="radio"]:checked');
  }

  getValue(val) {
    this.value = val;
  }

  get valueValidation() {
    return this.validationAction;
  }

  radioChange(event) {

    for (var i = 0; i < this.control.length; i++) {
      this.control[i].classList.remove('is-valid');
    }

    var radioEvent = event.target;
    if (radioEvent.checked) {
      this.getValue(radioEvent.value);

      this.controlChecked[0].classList.add('is-valid');
    }

    const obj = {};

    const paradatas = [];

    if (output.length > 0) {
      let dataLama = output.filter(x => x.componentId == this.componentId);

      if (dataLama.length > 0) {
        paradatas.push(this.currentDateTime);
        paradatas.push(this.value);

        var datapara = [];
        for (var i = 0; i < dataLama[0].paradata.length; i++) {
          datapara.push(dataLama[0].paradata[i]);
        }

        datapara.push(paradatas);

        var objBaru = {};

        var hasilObject = Object.assign(objBaru, {
          componentId: this.componentId,
          dataKey: this.dataKey,
          value: this.value,
          remark: this.valueremark,
          paradata: datapara
        });

        //update
        var objIndex = output.findIndex((obj => obj.componentId == this.componentId));
        output[objIndex] = hasilObject;
      } else {
        paradatas.push(this.currentDateTime);
        paradatas.push(this.value);


        var hasilObject = this.objectDataOutput(this.componentId, this.dataKey, this.value, this
          .valueremark,
          paradatas);

        output.push(hasilObject);
      }

    } else {
      paradatas.push(this.currentDateTime);
      paradatas.push(this.value);

      var hasilObject = this.objectDataOutput(this.componentId, this.dataKey, this.value, this
        .valueremark,
        paradatas);

      output.push(hasilObject);
    }
  }

  onChangeRemark(event) {
    let dataLama = output.filter(x => x.componentId == this.componentId);

    if (dataLama.length > 0) {

      var datapara = [];
      for (var i = 0; i < dataLama[0].paradata.length; i++) {
        datapara.push(dataLama[0].paradata[i]);
      }

      //datapara.push(paradatas);

      var objBaru = {};

      var hasilObject = Object.assign(objBaru, {
        componentId: this.componentId,
        dataKey: this.dataKey,
        value: this.value,
        remark: this.valueremark,
        paradata: datapara
      });

      //update
      var objIndex = output.findIndex((obj => obj.componentId == this.componentId));
      output[objIndex] = hasilObject;
    }
  }
}

class ComboBox extends Component {
  constructor(meta) {
    super(meta)
    this.control[0].addEventListener('change', this.onChange.bind(this));
    this.remarkcontrol.addEventListener('change', this.onChangeRemark.bind(this));
  }

  get control() {
    return this.element.querySelectorAll('select');
  }

  get value() {
    return this.control[0].value;
  }

  get valueValidation() {
    return this.validationAction;
  }

  // jaga"  (gak dipake)
  lamaonChange() {

    // if (this.hasParametersValidation) {

    // } else {
    //   this.control[0].classList.remove('is-valid');
    //   const valid = this.validationAction.filter(meta => !eval(meta.rules));

    //   if (valid.length > 0) {
    //     this.
    //   } else {
    //     this.showErrors();
    //   }
    // }

    let resultValidation = this.actionValidation;

    if (resultValidation.length > 0) {
      console.log(resultValidation[0].message);
    } else {
      console.log('udah bener isinya');
    }
    //console.log(this.hasParametersValidation);

    // let validation = [];
    // if (this.valueValidation[0].params != false) {
    //   const params = this.valueValidation[0].params;

    //   params.forEach(element => {
    //     //console.log(element)

    //     const valueParam = $('input[name=' + element + ']').val();

    //     validation = this.validationAction.map(meta => ({
    //       id: meta.id,
    //       rules: meta.rules.replace(element, "'" + valueParam + "'"),
    //       message: meta.message,
    //       type: meta.type,
    //       params: meta.params
    //     })).filter(x => eval(x.rules));


    //   });
    // }

    // console.log(validation)


    //this.control[0].classList.remove('is-valid');
    // if (this.valueValidation.length == 0) {
    //   if (this.value.toString() != "" || this.value != null) {
    //     this.hideErrors();
    //   } else {

    //     this.showErrors();
    //   }
    // }


    const paradatas = [];

    if (output.length > 0) {
      let dataLama = output.filter(x => x.componentId == this.componentId);

      if (dataLama.length > 0) {
        paradatas.push(this.currentDateTime);
        paradatas.push(this.value);

        var datapara = [];
        for (var i = 0; i < dataLama[0].paradata.length; i++) {
          datapara.push(dataLama[0].paradata[i]);
        }

        datapara.push(paradatas);

        var objBaru = {};

        var hasilObject = Object.assign(objBaru, {
          componentId: this.componentId,
          dataKey: this.dataKey,
          value: this.value,
          remark: this.valueremark,
          paradata: datapara
        });

        //update
        var objIndex = output.findIndex((obj => obj.componentId == this.componentId));
        output[objIndex] = hasilObject;
      } else {
        paradatas.push(this.currentDateTime);
        paradatas.push(this.value);


        var hasilObject = this.objectDataOutput(this.componentId, this.dataKey, this.value, this
          .valueremark,
          paradatas);

        output.push(hasilObject);
      }

    } else {
      paradatas.push(this.currentDateTime);
      paradatas.push(this.value);

      var hasilObject = this.objectDataOutput(this.componentId, this.dataKey, this.value, this
        .valueremark,
        paradatas);

      output.push(hasilObject);
    }
  }

  onChange() {
    let resultValidation = this.actionValidation;


    if (resultValidation.length > 0) {
      this.showErrors();
      //this.hideErrors();
    } else {

      this.hideErrors();
    }

    const paradatas = [];

    if (output.length > 0) {
      let dataLama = output.filter(x => x.componentId == this.componentId);

      if (dataLama.length > 0) {
        paradatas.push(this.currentDateTime);
        paradatas.push(this.value);

        var datapara = [];
        for (var i = 0; i < dataLama[0].paradata.length; i++) {
          datapara.push(dataLama[0].paradata[i]);
        }

        datapara.push(paradatas);

        var objBaru = {};

        var hasilObject = Object.assign(objBaru, {
          componentId: this.componentId,
          dataKey: this.dataKey,
          value: this.value,
          remark: this.valueremark,
          paradata: datapara
        });

        //update
        var objIndex = output.findIndex((obj => obj.componentId == this.componentId));
        output[objIndex] = hasilObject;
      } else {
        paradatas.push(this.currentDateTime);
        paradatas.push(this.value);


        var hasilObject = this.objectDataOutput(this.componentId, this.dataKey, this.value, this
          .valueremark,
          paradatas);

        output.push(hasilObject);
      }

    } else {
      paradatas.push(this.currentDateTime);
      paradatas.push(this.value);

      var hasilObject = this.objectDataOutput(this.componentId, this.dataKey, this.value, this
        .valueremark,
        paradatas);

      output.push(hasilObject);
    }
  }

  onChangeRemark(event) {
    let dataLama = output.filter(x => x.componentId == this.componentId);

    if (dataLama.length > 0) {

      var datapara = [];
      for (var i = 0; i < dataLama[0].paradata.length; i++) {
        datapara.push(dataLama[0].paradata[i]);
      }

      //datapara.push(paradatas);

      var objBaru = {};

      var hasilObject = Object.assign(objBaru, {
        componentId: this.componentId,
        dataKey: this.dataKey,
        value: this.value,
        remark: this.valueremark,
        paradata: datapara
      });

      //update
      var objIndex = output.findIndex((obj => obj.componentId == this.componentId));
      output[objIndex] = hasilObject;
    }
  }

  hideErrors() {

    this.control[0].classList.remove('is-invalid');
    this.control[0].classList.add('is-valid');

    const hasFeedBack = this.element.querySelector('.invalid-feedback') !== null;

    if (hasFeedBack)
      this.element.getElementsByClassName('invalid-feedback')[0].remove();
  }

  showErrors() {

    this.control[0].classList.remove('is-invalid');
    this.control[0].classList.add('is-invalid');


    const hasFeedBack = this.element.querySelector('.invalid-feedback') !== null;
    if (!hasFeedBack) {
      const invalidFeedback = document.createElement('div');


      let el = Object.assign(invalidFeedback, {
        className: 'invalid-feedback',
        textContent: this.actionValidation[0].message
      });

      this.control[0].parentElement.appendChild(el);
    } else {

      this.control[0].parentElement.lastChild.remove();

      const invalidFeedback = document.createElement('div');
      let el = Object.assign(invalidFeedback, {
        className: 'invalid-feedback',
        textContent: this.actionValidation[0].message
      });
      this.control[0].parentElement.appendChild(el);

    }
  }
}

class DateInput extends Component {
  constructor(meta) {
    super(meta)

    this.property.attr('type', 'date');
    // this.control.classList.add('datepicker');


    // console.log(this.control);
    this.control.addEventListener('change', this.onChange.bind(this));

    this.remarkcontrol.addEventListener('change', this.onChangeRemark.bind(this));


  }

  get control() {
    return this.element.querySelector('input[type=date]');
  }

  get value() {
    return this.control.value;
  }

  onChange(event) {
    //getAge(this.value);
    const paradatas = [];

    if (output.length > 0) {
      let dataLama = output.filter(x => x.componentId == this.componentId);

      if (dataLama.length > 0) {
        paradatas.push(this.currentDateTime);
        paradatas.push(this.value);

        var datapara = [];
        for (var i = 0; i < dataLama[0].paradata.length; i++) {
          datapara.push(dataLama[0].paradata[i]);
        }

        datapara.push(paradatas);

        var objBaru = {};

        var hasilObject = Object.assign(objBaru, {
          componentId: this.componentId,
          dataKey: this.dataKey,
          value: this.value,
          remark: this.valueremark,
          paradata: datapara
        });

        //update
        var objIndex = output.findIndex((obj => obj.componentId == this.componentId));
        output[objIndex] = hasilObject;
      } else {
        paradatas.push(this.currentDateTime);
        paradatas.push(this.value);

        var hasilObject = this.objectDataOutput(this.componentId, this.dataKey, this.value, this
          .valueremark, paradatas);

        output.push(hasilObject);
      }

    } else {
      paradatas.push(this.currentDateTime);
      paradatas.push(this.value);

      var hasilObject = this.objectDataOutput(this.componentId, this.dataKey, this.value, this
        .valueremark,
        paradatas);

      output.push(hasilObject);
    }
  }

  onChangeRemark(event) {
    let dataLama = output.filter(x => x.componentId == this.componentId);

    if (dataLama.length > 0) {

      var datapara = [];
      for (var i = 0; i < dataLama[0].paradata.length; i++) {
        datapara.push(dataLama[0].paradata[i]);
      }

      //datapara.push(paradatas);

      var objBaru = {};

      var hasilObject = Object.assign(objBaru, {
        componentId: this.componentId,
        dataKey: this.dataKey,
        value: this.value,
        remark: this.valueremark,
        paradata: datapara
      });

      //update
      var objIndex = output.findIndex((obj => obj.componentId == this.componentId));
      output[objIndex] = hasilObject;
    }
  }
}

function getAge(tgl_lhr) {
  const today = new Date();
  const birthOfDate = new Date(tgl_lhr);
  let age = today.getFullYear() - birthOfDate.getFullYear();

  const month = today.getMonth() - birthOfDate.getMonth();

  if (month < 0 || (month === 0 && today.getDate() < birthOfDate.getDate())) {
    age = age - 1;
  }

  return age;
}